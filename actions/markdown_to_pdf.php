<?php
	$id = $_GET["id"];

    // get article par son id
    $article = null;
    foreach($articles as $el) {
        if ($id == $el->id) {
            $article = $el;
            break;
        }
    }

    $content_dir = "content/".$id."/";
    $markdown_name = removeExt(getMarkdownName($content_dir));
	$markdown_file = $content_dir.$markdown_name.".md";
    $html_file = $content_dir.$markdown_name.".html";
    $pdf_file = $content_dir.$markdown_name.".pdf";
    /* pandoc */
    // $command = "pandoc -s -o ".$pdf_file." ".$markdown_file;
    /* weasyprint */
    // the -s option can add a filename for a user stylesheet
    $command = "weasyprint -s assets/css/print.css {$html_file} {$pdf_file}";
    exec($command);
?>
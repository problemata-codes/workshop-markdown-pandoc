<?php
	$input = $_FILES;
	echo "markdown to docx\n";
	print_r($input);

	$dir_content = "../exports/";

	if (isset($_FILES['document']) AND $_FILES['document']['error'] == 0) {
	    if ($_FILES['document']['size'] <= 10000000) {

	        $infosfichier = pathinfo($_FILES['document']['name']);
	        $extension_upload = $infosfichier['extension'];
	        $extensions_autorisees = array('md');

	        if (in_array($extension_upload, $extensions_autorisees)) {
			    // création dossier pour contenir : docx, json et md
			    $id = uniqid();
			    $target_dir = $dir_content.$id;
			    mkdir($target_dir);
			    
			    $target_file = basename($_FILES["document"]["name"]);
			    $target_ext = pathinfo($target_file, PATHINFO_EXTENSION); // extension fichier source
			    $target_filename = pathinfo($target_file, PATHINFO_FILENAME); // name sans extension
			    $target_filename = sanitizer($target_filename);
			    $target_file_url = $target_dir . "/" . $target_filename;

			    // écriture du docx dans le dossier
			    move_uploaded_file($_FILES['document']['tmp_name'], $target_file_url.".".$target_ext);

			    // conversion pandoc du fichier uploadé et écriture du .md
			    $target_pandoc = $target_file_url . ".docx";
			    $command = "pandoc " . $target_file_url.".".$target_ext . " -o " . $target_pandoc;
			    exec($command);
			}
		}
	}

	function sanitizer($string) {
	   $string = str_replace(' ', '-', $string);
	   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	   $string = strtolower($string);
	   return preg_replace('/-+/', '-', $string);
	}


?>
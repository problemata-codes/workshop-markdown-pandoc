<?php

	$value = file_get_contents('php://input');
	$data = json_decode($value);
	
	$post_content = $data->{'content'};
	$post_dir = $data->{'dir'};
	$post_titre = filter_var($data->{'titre'}, FILTER_SANITIZE_STRING);
	$post_mediateur = filter_var($data->{'mediateur'}, FILTER_SANITIZE_STRING);
	$post_id = $data->{'id'};

	$folder = scandir($post_dir);

	$json = (object) [
	    'titre' => $post_titre,
	    'mediateur' => $post_mediateur,
	    'id' => $post_id,
	];

	$current_md_file = null;
	foreach ($folder as $key => $value) {
		if(strpos($value, ".md") !== false){
		    $current_content = file_get_contents($post_dir."/".$value);
		    file_put_contents($post_dir."/".$value, $post_content);

		    // file_put_contents($data["dir"]."/test.json", "helo");
		   	file_put_contents($post_dir."/info.json", json_encode($json));
		    echo "Success";
		}
	}

?>
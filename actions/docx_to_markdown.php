<?php

/* DOSSIER DE STOCKAGE */

$dir_content = "../content/";



/* CRÉATION OBJ JSON */

if (isset($_POST["titre"])) {
    $titre = filter_var($_POST["titre"], FILTER_SANITIZE_STRING); 
} else {
    $titre = "";
}
if (isset($_POST["mediateur"])) {
    $mediateur = filter_var($_POST["mediateur"], FILTER_SANITIZE_STRING); 
} else {
    $mediateur = "";
}
$json = (object) [
    'titre' => $titre,
    'mediateur' => $mediateur,
];



/* UPLOAD / CONVERSION / ÉCRITURE */

if (isset($_FILES['document']) AND $_FILES['document']['error'] == 0) {
    if ($_FILES['document']['size'] <= 1000000) {

        $infosfichier = pathinfo($_FILES['document']['name']);
        $extension_upload = $infosfichier['extension'];
        $extensions_autorisees = array('docx');

        if (in_array($extension_upload, $extensions_autorisees)) {
            
            // création dossier pour contenir : docx, json et md
            $id = uniqid();
            $target_dir = $dir_content.$id;
            mkdir($target_dir);
            
            $target_file = basename($_FILES["document"]["name"]);
            $target_ext = pathinfo($target_file, PATHINFO_EXTENSION); // extension fichier source
            $target_filename = pathinfo($target_file, PATHINFO_FILENAME); // name sans extension
            $target_filename = sanitizer($target_filename);
            $target_file_url = $target_dir . "/" . $target_filename;

            // écriture du docx dans le dossier
            move_uploaded_file($_FILES['document']['tmp_name'], $target_file_url.".".$target_ext);

            // conversion pandoc du fichier uploadé et écriture du .md
            $target_pandoc = $target_file_url . ".md";
            $command = "pandoc --wrap=none " . $target_file_url.".".$target_ext . " -o " . $target_pandoc;
            exec($command);

            // ajout de id au json
            $json->{"id"} = $id;
            // écriture du .json
            $target_json = $target_dir . "/info.json";
            file_put_contents($target_json, json_encode($json));

            $medias_dir = $target_dir ."/medias";
            mkdir($medias_dir);

            echo "L'article '{$titre}' a été enregistré et converti en markdown.";
            header("Location: ../?page=editor&id=".$id);
        } else {
            echo "Erreur";
        }
    } else {
        echo "Fichier trop lourd";
    }
} else {
    echo "Erreur";
}

function sanitizer($string) {
   $string = str_replace(' ', '-', $string);
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
   $string = strtolower($string);
   return preg_replace('/-+/', '-', $string);
}

?>
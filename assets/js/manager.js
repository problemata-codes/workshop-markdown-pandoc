//console.log(": manager/core.js");
var max_length = 6;

/* repository listing */
loadRepository();

function loadRepository(){
	directory.raw = null;
	directory.raw_object.innerHTML = "";
	directory.list.innerHTML = "";
	var xhr = new XMLHttpRequest();
	xhr.onload = () => {
	    if (xhr.status == 200) {
	        directory.raw = xhr.response;
			directory.raw_object.innerHTML = directory.raw;
   	 		createList();
   	    } else {
	        console.error('Error', xhr.status);
	    }
	};
	xhr.open('GET', directory.url);
	xhr.send();
}

function createList(){
	var tr = directory.raw_object.getElementsByTagName("tbody")[0].getElementsByTagName("tr");
	for(var i = 0; i < tr.length-1; ++i){
		createNode(i,tr[i],directory.list);
	}
	activateClipboards();
}

function createNode(index,el,parent){
	if(index > 2){
		var item = document.createElement("li");

		if(el.getElementsByTagName("a")[0]){
			console.log(directory.url);
			var my_url = el.getElementsByTagName("a")[0].href.split(directory.url);
			////console.log(my_url);

			var obj = {
				index : index-3,
				url : directory.url,
				name : el.getElementsByTagName("a")[0].innerText,
				last_modified : null,
				type : null
			}
			obj.url += obj.name;
			//item.innerHTML = obj.index + " : <a href='"+obj.url+"'>" + obj.url + "</a>";
			////console.log(obj.url);

			obj.name = obj.name.replace(/\//g, '');
			obj.type = obj.name.split(".")[1];
			obj.last_modified = el.getElementsByTagName("td")[2].innerText;
			item.innerHTML =
				"<div class='item-tool'>"
					+"<button class='clipboard' data-clipboard-target='#clipboard-target-"+index+"' title='copier l&#39;URL'></button>"
					+"<button data-value='"+obj.name+"' data-url='' onclick='renameItem(this);' title='renommer' class='renamer'></button>"
					+"<button data-value='"+obj.name+"' data-url='' onclick='deleteItem(this);' title='supprimer' class='deleter'></button>"
				+"</div>"
				+"<div><a class='item-name' href='"+directory.url + obj.name+"' target='_blank'>" + obj.name + "</a></div>"
				+"<div><span class='item-date'>"+obj.last_modified+"</span></div>";

			item.setAttribute("data-parent",false);

			var clipContainer = document.getElementById("clip-container");
			clipContainer.innerHTML += "<li class='item-url clipboard-targets' id='clipboard-target-"+index+"'>"+obj.url+"</li>";


			item.setAttribute("data-type",obj.type);
			item.setAttribute("data-url",obj.url);
			item.setAttribute("data-name",obj.name);
			item.setAttribute("data-last-modified",obj.last_modified);
			item.className = "item";
			if(index%2==0){ item.classList.add("even");}

			var is_image = isImage(obj.name);
			if(is_image == true){
				var img_container = document.createElement("div");
				img_container.classList.add("img-container");
				img_container.innerHTML = "<a href='"+directory.url + obj.name+"' target='_blank'></a>";
				var img = document.createElement("img");
				// test lazyload natif browser
				img.setAttribute("loading", "lazy");
				img.src = directory.url + obj.name;
				img_container.firstChild.appendChild(img);
				item.appendChild(img_container);
			}

			var is_video = isVideo(obj.name);
			if(is_video == true){
				var vid_container = document.createElement("div");
				vid_container.classList.add("vid-container");
				vid_container.innerHTML = "<a href='"+directory.url + obj.name+"' target='_blank'></a>";
				var vid = document.createElement("video");
				vid.src = directory.url + obj.name;
				vid_container.firstChild.appendChild(vid);
				item.appendChild(vid_container);
			}
			var is_audio = isAudio(obj.name);
			if(is_audio == true){
				var audio_container = document.createElement("div");
				audio_container.classList.add("aud-container");
				var audio = document.createElement("audio");
				audio.src = directory.url + obj.name;
				audio.controls = true;
				audio_container.appendChild(audio);
				item.appendChild(audio_container);
			}

		}
		parent.appendChild(item);
	}
}
function isImage(filename) {
	if(!filename.match(/.(jpg|jpeg|png|gif)$/i)){ return false; }else{ return true; }
}
function isVideo(filename) {
	if(!filename.match(/.(mp4|avi|mov)$/i)){ return false; }else{ return true; }
}
function isAudio(filename) {
	if(!filename.match(/.(mp3|ogg|wav|flac)$/i)){ return false; }else{ return true; }
}

/*forms*/
function toggleForm(el){
	//console.log(":toggleForm");
	emptyAllForm();
	var lis = document.getElementById("form-controller").getElementsByTagName("li");
	for(var l = 0; l < lis.length; ++l){
		lis[l].classList.remove("active");
	}
	el.parentElement.classList.add("active");

	var forms = document.getElementsByClassName("form");
	for(var f = 0; f < forms.length; ++f){
		forms[f].classList.remove("active");
	}
	document.getElementById(el.getAttribute("data-target")).classList.add("active");
}
function emptyAllForm(){
	//console.log(":emptyAllForm");
	document.getElementById("delete-name").value = "";
	document.getElementById("new-name").value = "";
	document.getElementById("new-name").setAttribute("data-name",null);
	document.getElementById("name-name").innerText = "";

	//document.getElementById("folder-name").value = "";
	document.getElementById("file-name").value = "";

	//document.getElementById("mover-name").value = "";
	//document.getElementById("mover-target-url").value = "";
}
function deleteItem(el){
	console.log(":deleteItem");
	toggleForm(document.getElementById("button-deleter"));
	document.getElementById("delete-name").value = el.getAttribute("data-value");
}
function renameItem(el){
	//console.log(":renameItem");
	toggleForm(document.getElementById("button-renamer"));
	var name = el.getAttribute("data-value").split(".")[0];
	var ext = el.getAttribute("data-value").split(".")[1];
	document.getElementById("new-name").value = name;
	document.getElementById("new-name").setAttribute("data-name",name);
	document.getElementById("name-name").innerText = name;

	if(ext != undefined){
		document.getElementById("new-name").setAttribute("data-extension",ext);
		document.getElementById("name-extension").innerText = ext;
	}
	//console.log(document.getElementById("new-name"));
}

/* actions with payload */
function deleteFileOrFolder(el){
	console.log(":deleteFileOrFolder");
	var name = document.getElementById("delete-name").value;
	//var existing = checkExisting(name);
	checkExisting(name);


	if(name.length > 1){
		console.log("ok");
		var checks = [];
		var items = document.getElementsByClassName("item");
		for(var i = 0; i < items.length; ++i){
			if(items[i].getAttribute("data-name") == name){ checks.push(true); }else{ checks.push(false); }
		}
		if(checks.indexOf(true) != -1){
			payload.detail = {
				type : null,
				action : "delete",
				name : null,
				data : name
			};
			sendPayload();
			loadRepository();
			outputMsg("deletion-success");
			document.getElementById("delete-name").value = "";
		}else{
			outputMsg("not-existing");
		}
	}else{
		outputMsg("empty");
	}

}

function uploadFile(){
	//console.log(":uploadFile");
	sendFilePayload();
}
function sendFilePayload(){
   var files = document.getElementById("file-name").files;

   payload.detail = {
		type : null,
		action : "upload",
		name : null,
		data : null
   };

	if(files.length > 0 ){
		var formData = new FormData();
		formData.append('url', payload.url);
		formData.append('action', payload.detail.action);
		formData.append("file", files[0]);
		console.log(files[0]);

		var xhttp = new XMLHttpRequest();
		xhttp.open("POST", payload.script, true);

		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {
				var response = this.responseText;
				console.log(this.responseText);
				if(response == 1){
					outputMsg("upload-error");
				}else{
					outputMsg("upload-success");
				}
				loadRepository();
			}
		};
		xhttp.send(formData);
	}else{
	outputMsg("upload-select");
	}
}
function renameFileOrFolder(){
	//console.log(":renameFileOrFolder");

	var newname = document.getElementById("new-name").value;
	var oldname = document.getElementById("new-name").getAttribute("data-name");
	var ext = document.getElementById("new-name").getAttribute("data-extension");
	var complete = oldname;
	if(ext != null || ext != undefined){
		complete += "." + ext;
	}
	//console.log(complete, oldname, newname);

	var valid = checkValidity(complete);
		if(valid == "empty"){}
		else if(valid == "invalid"){}
		else{}

	/*var existing = checkExisting(complete);
	//console.log(complete,existing);*/
	if(newname.length > 0){
		if(oldname != null || oldname != "null" || oldname != undefined){
			if(newname != oldname){
				//perform
				payload.detail = {
					type : null,
					action : "rename",
					name : oldname,
					data : newname
				};
				if(ext != null || ext != undefined){
					payload.detail.ext = ext;
				}
				//console.log(payload.detail);
				if(payload.detail.name != "null" && payload.detail.name != null){
					sendPayload();
					loadRepository();
					outputMsg("rename-success");
				}else{
					outputMsg("not-existing");
				}
				document.getElementById("new-name").value = "";
				document.getElementById("new-name").setAttribute("data-name", null);
				document.getElementById("new-name").setAttribute("data-extension", null);
				document.getElementById("name-extension").innerText = "";
				document.getElementById("name-name").innerText = "";
			}else{
				outputMsg("rename-not-change");
			}
		}
	}
}

var payload = {
	url : document.getElementById("current_url").getAttribute("data-value"),
	script : url_manager+"/scripts.php",
	detail : {
		type : null,
		action : null,
		name : null,
		data : null
	}
};

function sendPayload(){
	//console.log("sending",payload.detail,"to",payload.url);

	var http = new XMLHttpRequest();
	var params = payloadToString(payload);

	http.open('POST', payload.script, true);

	http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	
	http.onreadystatechange = function() {//Call a function when the state changes.
	    if(http.readyState == 4 && http.status == 200) {
	    	//console.log(http.responseText);
	        outputAction(http.responseText);
	    }
	}
	http.send(params);
}


function payloadToString(payload){
	var str = "";
	var temp = [
		"url=" + payload.url
	];
	for(k in payload.detail){
		//console.log(k,":",payload.detail[k]);
		temp.push(k + "=" + payload.detail[k]);
	}
	return temp.join("&");
}
function outputAction(callback){
	//console.log(":outputAction");
	//console.log(callback);
	document.getElementById("php-output").innerHTML = "";
	//document.getElementById("php-output").innerHTML = callback;
}
function checkValidity(inputname){
	// function to compute textvalue
	// if empty, if contains spaces and special characters
	return inputname;
}
function checkExisting(inputname){
	var items = document.getElementsByClassName("item");
	var checks = [];
	for(var i = 1; i < items.length; ++i){
		////console.log(inputname, ":", items[i].getAttribute("data-name"));
		if(items[i].getAttribute("data-name") == inputname){ checks.push(true); }else{ checks.push(false); }
	}
	if(checks.indexOf(true) != -1){ return true; }else{ return false; }
}

var eltodisapeear = null;
function outputMsg(id){
	var msgs = document.getElementsByClassName("msg");
	for(var m = 0; m < msgs.length; ++m){
		msgs[m].parentElement.classList.remove("active");
	}
	eltodisapeear = document.getElementById(id).parentElement;
	document.getElementById(id).parentElement.classList.add("active");
	setTimeout(fadeAwayMsg,2000);
}
function fadeAwayMsg(){
	eltodisapeear.classList.remove("active");
}




// clipboard.js init script
function activateClipboards(){

    //console.log(': activateClipboards');
    var clipboards = document.getElementsByClassName("clipboard");

    for(var c = 0; c < clipboards.length; ++c){
    	var targ = clipboards[c];
	    var clipboardUrl = new ClipboardJS(targ);
	    clipboardUrl.on('success', function(e) {
	        //console.info('Action:', e.action);
	        //console.info('Text:', e.text);
	        console.info(e.text);
	        
	        e.trigger.classList.add("success");
	        setTimeout(function() {
	            this.classList.remove("success");
	          }.bind(e.trigger), 700);

	        e.clearSelection();
	    });
	    
	    clipboardUrl.on('error', function(e) {
	        //console.error('Action:', e.action);
	        //console.error('Trigger:', e.trigger);
	        
	        e.trigger.classList.add("error");
	        setTimeout(function() {
	            this.classList.remove("error");
	          }.bind(e.trigger), 700);
	    });
    }

}
var device = {
	event : eventChecker(),
	window : {
		w : 0, h : 0, iteration : 0, ratio : null
	}
};

if(document.body.getAttribute("data-page") == "detail"){
	window.addEventListener("DOMContentLoaded", computeMode);
	window.addEventListener("resize",computeMode);
}

function computeMode(e){
	getDeviceInfo();
	checkRatio(device.window.w,document.body);
}

/* breakpoint values */
var breakpoints = [
	[0, "xs"], 			/* Extra small devices (portrait phones, less than 576px) */
	[576, "s"],			/* Small devices (landscape phones, 576 and up) */
	[768, "m"],			/* Medium devices (tablets, 768px and up) */
	[992, "l"],			/* Large devices (desktops, 992px and up) */
	[1200, "xl"],		/* Extra large devices (large desktops, 1200px and up) */
	[1500, "xxl"]		/* Extra extra large devices (very large screens, 1500px and up) */
];
function checkRatio(attr,target){
	if(device.window.iteration < breakpoints.length-1){
		if(attr <= breakpoints[device.window.iteration+1][0]){
			device.window.ratio = breakpoints[device.window.iteration][1];
			device.window.iteration = 0;
		}else{
			device.window.iteration++;
			checkRatio(attr,target);
		}
	}else{
		device.window.iteration = 0;
		device.window.ratio = breakpoints[breakpoints.length-1][1];
	}
	target.setAttribute("data-ratio",device.window.ratio);
	device.window.iteration = 0;
}
function getDeviceInfo(e){
	device.window.w = window.innerWidth;
	device.window.h = window.innerHeight;
}
function eventChecker(){
	return "click";
}
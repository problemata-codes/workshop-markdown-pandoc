function saveMarkdown(){
	console.log(":saveMarkdown");
	markdown.content = easyMDE.value();

	var article = document.querySelector(".article");
	var titre = article.querySelectorAll("li")[1].innerHTML;
	var mediateur = article.querySelectorAll("li")[2].innerHTML;
	markdown.titre = titre;
	markdown.mediateur = mediateur;
	sendContent();
}


async function sendContent() {
	var body = JSON.stringify(markdown);
    var response = await request(markdown.script, body, "POST");
    outputSaveMsg(response);
}

async function request(url, params, method = 'GET') {
    var options = {
      method,
      body: params,
      mode: "cors"
    };
    console.log(options);
    
    var response = await fetch(url, options);
    if (response.ok) { 
    	var text = await response.text();
    	return text;
    } else if (response.status !== 200) {
    	return generateErrorResponse('The server responded with an unexpected status.');
    }
 
}


function outputSaveMsg(response){
	document.getElementById("save-msg").innerHTML = response;
	setTimeout(fadeAwaySaveMsg,2000);
}
function fadeAwaySaveMsg(){
	document.getElementById("save-msg").innerHTML = "";
}

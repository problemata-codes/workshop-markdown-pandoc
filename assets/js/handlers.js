var handlers = document.getElementsByClassName("handler");
var zones = document.getElementsByClassName("zone");

var data = {
	x : null, y : null, px : 0, py : 0, dx : 0, dy : 0,
	target : null, tx : 0, ty : 0,
	min : 5, max : 95
};

initHandlers();

function initHandlers(){
	console.log(":: initHandlers =", handlers.length, "handler(s)");
	for(var h = 0; h < handlers.length; ++h){ updateHandler(handlers[h]); }
	window.onmousedown = handlerDown;
	window.onmousemove = handlerMove;
	window.onmouseup = handlerUp;
}

function handlerDown(e){
	if(e.target.classList.contains("handler")){
		data.target = e.target;
		data.px = e.clientX; data.py = e.clientY;
		data.tx = data.target.getAttribute("data-x"); data.ty = data.target.getAttribute("data-y");
		window.onselectstart = function(e){ return false; };
		preventFrames("none");
	}
}

function handlerMove(e){
	if(data.target != null){
		data.x = e.clientX; data.y = e.clientY;
		data.dx = data.px - data.x; data.dy = data.py - data.y;
		var x = (data.tx-pxToPercent("x",data.dx)), y = (data.ty-pxToPercent("y",data.dy));
		if(x <= data.min){ x = data.min; } if(x >= data.max){ x = data.max; }
		if(y <= data.min){ y = data.min; } if(y >= data.max){ y = data.max; }
		data.target.setAttribute("data-x", x);
		data.target.setAttribute("data-y", y);
		updateHandler(data.target);
	}
}

function handlerUp(e){
	if(data.target != null){
		data.x = null; data.y = null; data.px = 0; data.py = 0; data.dx = 0; data.dy = 0;
		data.target = null; data.tx = 0; data.ty = 0;
	}
	window.onselectstart = function(e){ return true; };
	preventFrames("all");
}

function pxToPercent(axis,val){
	var ref = document.getElementsByTagName("main")[0];
	var refdata = { x : ref.offsetWidth, y : ref.offsetHeight };
	var result = (val * 100) / refdata[axis];
	return result;
}

function updateHandler(el){
	var x = el.getAttribute("data-x"), y = el.getAttribute("data-y"), dir = el.getAttribute("data-dir"), w = el.offsetWidth;

	if(dir == "x"){
		el.style.left = x + "%";
		updateTarget("input","width",x+"%");
		updateTarget("output","width","calc("+(100-x)+"% - (var(--handler-size)))");
		updateTarget("output","left","calc("+x+"% + (var(--handler-size)))");
		updateTarget("frame","width","100%");
		updateTarget("frame","height","100%");
	}
	if(dir == "y"){ el.style.top = y + "%"; }
	if(dir == "both"){ el.style.top = y + "%"; el.style.left = x + "%"; }
}
function updateTarget(id,attr,value){
	document.getElementById(id).style[attr] = value;
}
function preventFrames(state){
	var frames = document.getElementsByTagName("iframe");
	for(var f = 0; f < frames.length; ++f){ frames[f].style.pointerEvents = state; }
}
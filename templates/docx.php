<main>
	<form action="actions/docx_to_markdown.php" method="post" enctype="multipart/form-data">
        <h1>Créer un nouvel article</h1>
        <ul>
            <li>
                <label>Titre</label>
                <input type="text" name="titre" autocomplete='off' required>
            </li>
            <li>
                <label>Médiateur</label>
                <input type="text" name="mediateur" autocomplete='off' required>
            </li>
            <li>
                <label>Fichier à importer</label>
                <input type="file" name="document" id="doc_loader" accept=".xlsx, .xls, .doc, .docx, .ppt, .pptx, .txt, .pdf" autocomplete='off' required>
            </li>
            <li>
                <input type="submit" name="converter" value="Créer">
            </li>
        </ul>
    </form>
</main>

<main>
	<form action="actions/folder_remover.php" method="post">
		<input type="hidden" name="id" value="<?= $_GET["id"] ?>">
		<p>Supprimer l'objet <em id="name"><?= $_GET["id"] ?></em> ?</p>
		<?php 
			include "utils.php";
			$folder = "content";
			$infos = getFolderData($folder."/".$_GET["id"]);
		?>
		<ul>
			<li><span class="label">id</span><span><?= $infos["id"] ?></span></li>
			<li><span class="label">titre</span><span><?= $infos["titre"] ?></span></li>
			<li><span class="label">médiateur</span><span><?= $infos["mediateur"] ?></span></li>
		</ul>
		<input type="submit" name="submit" value="Valider">
		<a href="?page=index"><span class="cancel">Annuler</span></a>
	</form>
</main>
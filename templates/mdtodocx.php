<main>
	<form action="../actions/markdown_to_docx.php" method="post" enctype="multipart/form-data">
        <h1>md to docx</h1>
        <ul>
            <li>
                <label>Fichier à importer</label>
                <input type="file" name="document" id="doc_loader" accept=".md" autocomplete='off' required>
            </li>
            <li>
                <input type="submit" name="converter" value="Créer">
            </li>
        </ul>
    </form>
</main>

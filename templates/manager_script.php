<?php
	/*print_r($_SERVER["HTTP_ORIGIN"]);
	echo "<br>";
	print_r($_SERVER["REQUEST_URI"]);
	echo "<br>";*/
	$payload = $_POST;
	checkAndComputePayload($payload);

	function checkAndComputePayload($data){
		if($data["action"] == "delete"){
			if($data["name"] != ""){
				delete_dir_r($data);
			}
		}
		if($data["action"] == "rename"){
			if($data["name"] != ""){
				renameDir($data);
			}
		}
		if($data["action"] == "upload"){
			uploadNewFile($data);
		}
	}

	// rename dir
	function renameDir($data){
		print_r($data);
		$old = "../".$data["url"].$data["name"].".".$data["ext"];
		$new = "../".$data["url"].$data["data"].".".$data["ext"];
		if(rename($old, $new)){
		   echo "Directory has been renamed";
		} else {
		   echo "Fail to rename directory";
		}
	}
	// delete dir (that deals with directories recursively)
	function delete_dir_r($data){
		print_r($data);
		$target = "../".$data["url"].$data["data"];
	    if(is_dir($target)){
	        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
	        foreach($files as $file){
	            delete_dir_r($file);      
	        }
	        rmdir($target);
	    }elseif(is_file($target)) {
	        unlink($target);
	    }
	}
	// upload file (1 by 1)
	function uploadNewFile($data){
		if(isset($_FILES['file']['name'])){
		   $filename = $_FILES['file']['name'];
		   $location = "../".$data["url"].$filename;
		   $file_extension = pathinfo($location, PATHINFO_EXTENSION);
		   $file_extension = strtolower($file_extension);
		   $valid_ext = array("tiff","pdf","doc","docx","jpg","jpeg","JPG","JPEG","png","jpeg","mp3","ogg","mp4","avi","gif","csv","json","md","html","css","js","wav");

		   print_r($_FILES);
		   echo "\n";
		   echo $filename ."\n";
		   echo $location ."\n";
		   echo $file_extension ."\n";

			if($_FILES['file']['error'] === UPLOAD_ERR_OK) {
				//uploading successfully done
				}else{
				throw new UploadException($_FILES['file']['error']);
			} 
			
			$response = 0;
			if(in_array($file_extension,$valid_ext)){
			  if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
			     $response = 1;
			  } 
			}
			echo $response;
			exit;
		}
	}

	class UploadException extends Exception{
	    public function __construct($code){
	        $message = $this->codeToMessage($code);
	        parent::__construct($message, $code);
	    }

	    private function codeToMessage($code){
	        switch ($code){
	            case UPLOAD_ERR_INI_SIZE:
	                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
	                break;
	            case UPLOAD_ERR_FORM_SIZE:
	                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
	                break;
	            case UPLOAD_ERR_PARTIAL:
	                $message = "The uploaded file was only partially uploaded";
	                break;
	            case UPLOAD_ERR_NO_FILE:
	                $message = "No file was uploaded";
	                break;
	            case UPLOAD_ERR_NO_TMP_DIR:
	                $message = "Missing a temporary folder";
	                break;
	            case UPLOAD_ERR_CANT_WRITE:
	                $message = "Failed to write file to disk";
	                break;
	            case UPLOAD_ERR_EXTENSION:
	                $message = "File upload stopped by extension";
	                break;

	            default:
	                $message = "Unknown upload error";
	                break;
	        }
	        return $message;
	    }
	}
?>
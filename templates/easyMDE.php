<?php

require_once '../core/Article.php';

// dossier de stockage
$dir_content = "../content/";

if(isset($_GET["id"])){
    $id = $_GET["id"];
} else {
    $id = "undefined";
}

$article = new Article($dir_content.$id);

?>  

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8;">
        <meta http-equiv="cache-control" content="no-cache">
        <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, shrink-to-fit=no">
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../assets/css/core.css">
        <link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
        <script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
        <script src="../assets/js/custom_mde.js"></script>
        <style>
            body, html{ display: block; margin: 0; padding: 0; }
            body{
              background-color: rgb(240,240,240);
              overflow-y: auto; overflow-x: hidden;
            }
            div.info{
              margin: 0.5rem;
            }
            .EasyMDEContainer{
              height: 100%;
              /*margin: 0 8px;*/
              background-color: white;
            }
            #save-btn{
              cursor: pointer;
              border-radius: 4px;
              background-color: gainsboro;
              font-family: sans-serif, Arial, Helvetica;
              font-size: 0.8rem;
            }
            #save-btn:hover{
              background-color: grey;
            }
            #save-msg:before{
              content: "php : ";
            }
            .cm-null, .cm-spell-error{ background-color: transparent; }
            .CodeMirror .cm-spell-error:not(.cm-url):not(.cm-comment):not(.cm-tag):not(.cm-word){ background-color: transparent; }
        </style>
    </head>

    <body>
      <div class="info">
        <ul>
            <li class="caption">
                <ul class="row">
                    <li>Identifiant</li>
                    <li>Titre</li>
                    <li>Médiateur</li>
                </ul>
            </li>
            <li class="article">
                <ul class="row">
                    <li><?= $article->id ?></li>
                    <li contenteditable><?= $article->titre ?></li>
                    <li contenteditable><?= $article->mediateur ?></li>
                </ul>
            </li>
            <li class="caption">
              <ul class="row">
                <li id="save-btn" onclick="saveMarkdown();">Sauvegarder</li>
                <li id="save-msg" style="background-color: transparent; border: 1px dotted black; font-style: italic;"></li>
              </ul>
            </li>
        </ul>
      </div>
      <textarea autocomplete='off'><?= $article->markdown ?></textarea>
      <script>
          var markdown = {
            script : "../actions/update_markdown.php",
            dir : "<?= $dir_content ?><?= $article->id ?>",
            id : "<?= $article->id ?>",
            content : null
          };
          // suppr pr l'instant les réglages autosave et forceSync car ils supprimaient le contenu ajouté
          var easyMDE = new EasyMDE({
            autoDownloadFontAwesome : true,
            autofocus : true,
            lineNumbers : true,
            indentWithTabs: false,
            minHeight : 1 + "rem",
            hideIcons: [],
            status: ["autosave", "lines", "words", "cursor"],
            tabSize: 4,
            uploadImage : true,
            sideBySideFullscreen: false,
            syncSideBySidePreviewScroll: false,
            spellChecker: false,
            spellCheck : false
        });

        window.onkeydown = disableKeyCombination;
        function disableKeyCombination(e){
            e = e || window.event;
            if(e.ctrlKey){
                var c = e.which || e.keyCode;
                switch (c) {
                    case 83: //ctrl+s
                        e.preventDefault();     
                        e.stopPropagation();
                        saveMarkdown();
                    break;
                }
            }
        }

      </script>
</body>


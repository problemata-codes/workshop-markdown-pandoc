<?php
	$c_editor = "";
	$c_detail_html = "";
	$c_detail_pdf= "";
	$c_delete = "";
	$page = "";
	if(isset($_GET["page"])){
		$page = $_GET["page"];
	}
	if($page == "index"){
		$c_editor = "active";
		$c_detail_html = "active";
		$c_detail_pdf = "active";
		$c_delete = "active";
	};
	if($page == "editor"){ $c_editor = "active"; };
	if($page == "detail"){
		if($_GET["format"] == "html"){
			$c_detail_html = "active";
		}else{
			$c_detail_pdf = "active";
		}
	}
?>
<style>
	.row > li > a{
		display: none;
	}
	.row > li > a.active{
		display: inline-block;
	}
</style>
<h2>Liste des articles</h2>
<ul class="list_articles">
	<!--
	<li>
		<input type="field" name="" placeholder="rechercher dans la liste" id="searcher" oninput="computeText();">
	</li>
	<script src="assets/js/text-input.js"></script>
	-->
	<li class="caption">
		<ul class="row">
			<li>Identifiant</li>
			<li>Titre</li>
			<li>Médiateur</li>
			<li>Actions</li>
		</ul>
	</li>
	<?php foreach ($articles as $article): ?>
	<li class="article" data-title="<?= $article->titre ?>" data-agent="<?= $article->mediateur ?>" data-id="<?= $article->id ?>">
		<ul class="row">
			<li><?= $article->id ?></li>
			<li><?= $article->titre ?></li>
			<li><?= $article->mediateur ?></li>
			<li>
				<a href="?page=editor&id=<?=$article->id?>" class="<?= $c_editor ?>"><button>éditer en markdown</button></a>
				<a href="?page=detail&format=html&id=<?=$article->id?>" class="<?= $c_detail_html ?>"><button>générer en html</button></a>
				<a href="?page=detail&format=pdf&id=<?=$article->id?>" class="<?= $c_detail_pdf ?>"><button>générer en pdf</button></a>
				<a href="?page=delete&id=<?=$article->id?>" class="<?= $c_delete ?>"><button>supprimer</button></a>
			</li>
		</ul>
	</li>
	<?php endforeach; ?>
</ul>

<!-- BEGIN CHECK -->
<?php
	$valid = false;
	if(isset($_GET["id"])){
		$id = $_GET["id"];
		$valid = true;
	} else {
		$id = "undefined";
	}
?>
<!-- END CHECK-->

<!-- BEGIN RENDER -->
<?php 
	if($valid == true){
?>
	<main style="padding: 0;">
	    <div id="input">
	        <iframe id="frame" src="templates/easyMDE.php?id=<?=$id?>"></iframe>
	    </div>
	    <div id="output" class="manager">
	    	<?php include "manager.php" ?>
	    </div>
	</main>
	<aside>
	    <div id="handler" class="handler" data-x="50" data-y="0" data-dir="x"><button></button></div>
	</aside>
    <?php include "templates/footer.php"; ?>
<?php
	}else{
?>
<main>
	<?php include "selector.php" ?>
</main>
<?php
	}
?>
<!-- END RENDER -->

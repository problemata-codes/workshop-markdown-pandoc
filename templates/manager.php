<?php
    $url_manager = "templates/manager_script.php";
    $id = explode("/", $dir)[1];

    $dir .= "/medias/";

    // debug pr un seul article pour /media
    $id = "602533ee0971a";
    // $dir = "/ws/workshop-markdown-pandoc/content/602533ee0971a/medias";
    $dir = "content/602533ee0971a/medias/";

    echo "<script>var obj_id = '".$id."'; console.log('id :',obj_id);</script>";
    echo "<script>var url_manager = '".$url_manager."';</script>";

    // get article par son id
    /*
    $id = "602533ee0971a";
    $article = null;
    foreach($articles as $el) {
        if ($id == $el->id) {
            $article = $el;
            break;
        }
    }
    */

?>

<input type="field" name="" placeholder="rechercher dans ce dossier" id="searcher" oninput="computeText();">
<div class="header">
    <div>
        <ul class="links-container">      
            <li class="links"><a href="?page=detail&id=<?= $id ?>&format=html">générer html</a></li>
            <li class="links"><a href="?page=detail&id=<?= $id ?>&format=pdf">générer pdf</a></li>
        </ul>
        <div type="field" id="current_url" data-value="<?= $dir ?>"><?= $dir ?></div>
        <ul id="form-controller">
            <li class="active"><button onclick="toggleForm(this);emptyAllForm();" data-target="hide-all" id="toggler-all"><strong>Actions :</strong></button></li>
            <li><button onclick="toggleForm(this);" data-target="create-file">ajouter</button></li>
            <li><button onclick="toggleForm(this);" data-target="rename-item" id="button-renamer">renommer</button></li>
            <li><button onclick="toggleForm(this);" data-target="delete-item" id="button-deleter">supprimer</button></li>
        </ul>
        <ul>
            <li id="hide-all" class="form active">
                <ul><li><button onclick="loadRepository();">&nbsp;Actualiser&nbsp;</button></li></ul>
            </li>
            <li id="create-file" class="form">
                <h4>Ajouter un fichier</h4>
                <ul>
                    <li><input type="file" name="file" id="file-name"></li>
                    <li><input type="button" name="" value="Uploader" onclick="uploadFile();" id="btn-upload"></li>
                </ul>
            </li>
            <li id="rename-item" class="form">
                <h4>Renommer un fichier existant</h4>
                <ul>
                    <li><input type="field" id="new-name" placeholder="nouveau nom du fichier" value="">&nbsp; <i>Nom actuel :</i> <span id="name-name"></span><span id="name-extension"></span></li>
                    <li><input type="button" name="" value="Valider" onclick="renameFileOrFolder(this);" id="deletion-validator" class="validator"><input type="button" id="deletion-canceler" class="canceler" name="" value="Annuler" onclick="toggleForm(document.getElementById('toggler-all'));emptyAllForm();"></li>
                </ul>
            </li>
            <li id="delete-item" class="form">
                <h4>Supprimer un fichier existant</h4>
                <ul>
                    <li>Êtes-vous sûr de vouloir supprimer <input type="field" id="delete-name" value=""></span>?</li>
                    <li><input type="button" name="" value="Valider" onclick="deleteFileOrFolder(this);" id="name-validator" class="validator"><input type="button" id="name-canceler" class="canceler" name="" value="Annuler" onclick="toggleForm(document.getElementById('toggler-all'));emptyAllForm();"></li>
                </ul>
            </li>
        </ul>
        <ul id="output-msg">
            <li class="msg"><p class="msgs form-empty" id="empty">Please specify a name or select an object</p></li>
            <li class="msg"><p class="msgs form-abort" id="existing"><span class="form-item-name"></span> File already existing</p></li>
            <li class="msg"><p class="msgs form-abort" id="not-existing"><span class="form-item-name"></span> File does not exist</p></li>
            <li class="msg"><p class="msgs form-success" id="creation-success"><span class="form-item-name"></span> File created successfully</p></li>
            <li class="msg"><p class="msgs form-error" id="creation-error">Error while creating <span class="form-item-name"></span> File</p></li>
            <li class="msg"><p class="msgs form-success" id="rename-success"><span class="form-item-name"></span> File renamed successfully</p></li>
            <li class="msg"><p class="msgs form-error" id="rename-not-change"><span class="form-item-name"></span>No modifications in filename</p></li>
            <li class="msg"><p class="msgs form-success" id="deletion-success"><span class="form-item-name"></span> File deleted successfully</p></li>
            <li class="msg"><p class="msgs form-error" id="deletion-error"><span class="form-item-name"></span> File deleted successfully</p></li>
            <li class="msg"><p class="msgs form-process" id="upload-begin">Started upload of <span class="form-item-name"></span> file</p></li>
            <li class="msg"><p class="msgs form-process" id="upload-process">Uploading : <span id="upload-percentage"></span> / 100 %</p></li>
            <li class="msg"><p class="msgs form-success" id="upload-success"><span class="form-item-name"></span> File uploaded successfully</p></li>
            <li class="msg"><p class="msgs form-error" id="upload-error">Error while creating <span class="form-item-name"></span> file</p></li>
            <li class="msg"><p class="msgs form-empty" id="upload-select">Please select a file</p></li>
            <li class="msg"><p class="msgs form-empty" id="move-not-change">No modifications in path</p></li>
            <li class="msg"><p class="msgs form-success" id="move-success">File successfully moved</p></li>
        </ul>
    </div>         
</div>
<div class="main">
    <div id="directory">
        <div id="directory-raw" style="display: none;"></div>
        <ul id="directory-list"></ul>
        <div id="php-output"></div>
    </div>
</div>
<div class="footer">
    <ul id="clip-container"></ul>
</div>

<script>
    var directory = {
        id : obj_id,
        url : "<?= $dir ?>",
        raw : null,
        raw_object : document.getElementById("directory-raw"),
        list : document.getElementById("directory-list"),
        files : []
    };
    console.log(directory);
</script>

<script src="assets/js/manager.js"></script>
<script src="assets/js/text-input.js"></script>

<?php include "core/Parsedown.php"; ?>
<?php include "core/ParsedownExtra.php"; ?>
<?php include "core/MyParsedown.php"; ?>

<?php
	$doc = null;
	if(isset($_GET["id"])){
?>

<?php
	$format = $_GET["format"];

	include "utils.php";
	$folder = "content";
	$infos = getFolderData($folder."/".$_GET["id"]);

	if($format == "html"){
		// convert md->html
		$html_content = null;
		include "actions/markdown_to_html.php";
?>
	<main>
		<h1>Détail (<?= $_GET["format"] ?>)</h1>
		<a href="?page=editor&id=<?= $_GET['id']?>"><button>éditer en markdown</button></a>
		<a href="?page=detail&format=pdf&id=<?= $_GET['id']?>"><button>générer en pdf</button></a>
		<a href="?page=delete&id=<?= $_GET['id']?>"><button>supprimer</button></a>
		<ul>
			<li><span class="label">id</span><span><?= $infos["id"] ?></span></li>
			<li><span class="label">titre</span><span><?= $infos["titre"] ?></span></li>
			<li><span class="label">médiateur</span><span><?= $infos["mediateur"] ?></span></li>
		</ul>
		<div id="html">
			<article>
				<h1><?= $infos["titre"] ?></h1>
				<h2><?= $infos["mediateur"] ?></h2>
				<?= $article_html; ?>
			</article>
		</div>
	</main>
<?php
	}
	if($format == "pdf"){
		// convert md->pdf
		$pdf_content = null;
		include "actions/markdown_to_pdf.php";
?>
	<main>
		<h1>Détail (<?= $_GET["format"] ?>)</h1>
		<a href="?page=editor&id=<?= $_GET['id']?>"><button>éditer en markdown</button></a>
		<a href="?page=detail&format=html&id=<?= $_GET['id']?>"><button>générer en html</button></a>
		<a href="?page=delete&id=<?= $_GET['id']?>"><button>supprimer</button></a>
		<ul>
			<li><span class="label">id</span><span><?= $infos["id"] ?></span></li>
			<li><span class="label">titre</span><span><?= $infos["titre"] ?></span></li>
			<li><span class="label">médiateur</span><span><?= $infos["mediateur"] ?></span></li>
		</ul>
		<div id="pdf">
			<div id="command"><?= $command ?></div>
			<div id="pdf_output">output pdf</div>

			<?php 
			$pattern = "{$article->path}/*.pdf";
        	$pdf_path = glob($pattern)[0];
			?>
			<iframe src="<?=$pdf_path?>"></iframe>
		</div>
	</main>
<?php
	}
?>

<?php
	}else{
?>
	<main>
		<?php include "selector.php"; ?>
	</main>
<?php
	}
?>
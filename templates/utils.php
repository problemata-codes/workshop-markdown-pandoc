<?php
	function getFolderData($path){
		$id = explode("/", $path)[1];
		$file = $path."/info.json";
		$data = json_decode(file_get_contents($file),true);
		return $data;
	}
	
	function delete_dir_r($target) {
	    if(is_dir($target)){
	        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
	        foreach($files as $file){
	            delete_dir_r($file);      
	        }
	        rmdir($target);
	    } elseif(is_file($target)) {
	        unlink($target);
	    }
	}
    function getMarkdownName($input_dir){
    	$files = scandir($input_dir);
    	foreach ($files as $key => $value) { if(strpos($value, ".md") !== false){ return $value; } }
    }
    function removeExt($filename){
    	$temp = explode(".", $filename);
    	return $temp[0];
    }
?>
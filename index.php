<?php
    $redirection = "?page=index";
    $pages = array(
        "editor",
        "docx",
        "index",
        "detail",
        "delete"
    );

/* DOSSIER DE STOCKAGE */
$dir_content = "content/";

/* CRÉATION DES OBJETS ARTICLES */
require_once 'core/Article.php';
// tous les dossiers de content/
$dirs = glob($dir_content."*", GLOB_ONLYDIR);
$articles = array();

// création d'un objet Article pour chaque dossier
foreach ($dirs as $dir) {
    $article = new Article($dir);
    $articles[] = $article;
}


?>

<!DOCTYPE html>
<html>
    <head>
        <!-- META -->
        <meta http-equiv="content-type" content="text/html; charset=UTF-8;">
        <meta http-equiv="cache-control" content="no-cache">
        <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, shrink-to-fit=no">
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="assets/css/core.css">
        <link rel="stylesheet" type="text/css" href="assets/css/manager.css">
        <script type="text/javascript" src="assets/js/handlers.js" defer></script>
        <script type="text/javascript" src="assets/js/clipboard.js" defer></script>
        <?php if($_GET['page'] == "detail" && isset($_GET["id"])){
        ?>
            <link rel="stylesheet" type="text/css" href="assets/css/detail.css">
            <script type="text/javascript" src="assets/js/detail.js" defer></script>
        <?php
        } ?>
    </head>

    <body data-page="<?= $_GET['page'] ?>" data-format="<?php if(isset($_GET['format'])){ echo $_GET['format']; } ?>">
        <?php include "templates/header.php"; ?>
        <?php
            if(isset($_GET["page"])){
                if(in_array($_GET["page"],$pages)){
                    include "templates/".$_GET["page"].".php";
                }else{
                    include "templates/404.html";
                }
            }else{
                header("Location: ".$redirection);
                exit();
            }
        ?>
    </body>
</html>

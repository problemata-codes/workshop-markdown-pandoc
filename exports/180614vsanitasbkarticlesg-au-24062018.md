<!-- todo: replace esperluette -->
<!-- todo: css blockquote -->
<!-- todo: css footnote -->
<!-- todo: css indent ul/ol/li -->
<!-- todo: css figure, figcaption ul li -->
<!-- todo: js manager : input search hide items -->



<!-- quelle règle pour les espaces et les ponctuation ? -->
<!-- L:108: retour a la ligne très problématique -->
<!-- todo : régler les appels de notes et les notes -->
<!-- todo : styles et balises dans les notes et les références bibliographiques, filmographiques, expositions -->

## Table des matières
<!-- que faire pour générer une table des matières qui prend en compte le système de notation hiérarchique des parties du texte ?(i, ii, iii vs 1, 2, 3 vs A, B, C vs a, b, c) -->
1. [Récit d'Histoire(s)](#i-recit-d-histoires)
    - [Archéologie d'une exposition](#b-archeologie-dune-exposition)
    - [Depuis la marge](#c-depuis-la-marge)
2. [Espace repoussoir](#ii-espace-repoussoir)
    - [Musée de la guerre](#a-musee-de-la-guerre)
    - [Sainte Bernadette du Banlay](#b-sainte-bernadette-du-banlay)
    - [Exposition manifeste](#c-exposition-manifeste)
3. [Montrer l'écriture](#iii-montrer-l-ecriture)
    - [Actualisation](#a-actualisation)
    - [Le desin graphique et le dessin littéraire](#b-le-dessin-graphique-et-le-dessin-litteraire)
    - [Un art révélationnaire](#c-un-art-revelationnaire)
4. [Bibliographie](#bibliographie)
    - [Ouvrages](#ouvrages)
    - [Filmographie](#filmographie)
    - [Exposition](#exposition)
    - [Notes](#notes)
5. [Table des figures](#table-des-figures)
    - [fig.1](#fig-1)
    - [fig.2](#fig-2)
    - [fig.3](#fig-3)
    - [fig.4](#fig-4)
    - [fig.5](#fig-5)
    - [fig.6](#fig-6)
    - [fig.7](#fig-7)
    - [fig.8](#fig-8)
    - [fig.9](#fig-9)
    - [fig.10](#fig-10)

<!-- from bold to h2 -->
## I - Récit d’Histoire(s) {#i-recit-d-histoires}

<!-- from nothing to h3 -->
<!-- que faudrait-il faire pour que la hiérarchie en points alphabétiques -->
### a. Histoire d’une enquête

<!-- ajouter blockquote tag dans detail.css -->

> « Pendant ma jeunesse, le littoral européen était interdit au public pour cause de travaux ; on y bâtissait un mur et je ne découvris l’Océan, dans l’estuaire de la Loire, qu’au cours de l’été 1945[^1]. »

<!-- faut-il ici laisser un renvoi vers la note ou alors créer une classe référence tag=ahref:(date) ? Wuelle syntaxe pour les références bibliographiques ? -->

Paul Virilio est un « enfant de la guerre ». Né en 1932, il en a connu
les conflits, les impacts, les dénouements et les conséquences. Il a
connu la Seconde Guerre mondiale depuis Nantes où il vivait à cette
période. Proche de la mer, il n’a pu la voir que tardivement. Les murs
de défense bloquaient l’horizon. Une fois celui-ci ouvert, il découvre
les plages de l’Atlantique, ça et là, des restes de la guerre. Des
architectures, des morceaux du conflit rendus obsolètes par la fin de la
guerre. Par la paix. C’est ce regard surpris et fasciné qu’expose
*Bunker archéologie* en 1975 au musée des Arts décoratifs de la ville de
Paris. Un regard d’abord sensible puis travaillé par ses études
d’architecture. L’environnement quotidien, celui des plages, des dunes,
des rivages du bord de l’Atlantique est devenu objet d’étude et
« L’architecture militaire du Mur de l’Atlantique » sujet de thèse. Pour
raconter l’immensité de ce mur, le poids de ces architectures et de
cette période dans l’histoire contemporaine, Paul Virilio commence un
voyage photographique en 1958. Il longe l’océan ou le mur, se rend de
fort en fort, de bunker en bunker, allant jusqu’aux côtes anglaises. La
photo est son outil principal. Elle lui permet de produire les images de
sa pensée, il systématise par le format et le noir et blanc, isole les
détails par le cadrage. Le chercheur catégorise les bunkers qu’il
rencontre selon leurs modalités d’insertion dans leur environnement,
leurs formes ou leur dégradation. Il produit des familles
architecturales qui racontent différentes facettes de la guerre : la
défense, l’impact ou encore la disparition. Les photographies
explicitent les similitudes ou les différences et les rendent évidentes
pour un lecteur ou pour le visiteur, celui de l’exposition *Bunker
archéologie*.

<!-- from nothing to h3 -->
### b. Archéologie d’une exposition {#b-archeologie-dune-exposition}

*Bunker archéologie* a lieu du 10 décembre 1975 au 29 février 1976, soit
17 ans après le début des explorations de l’architecture militaire par
Paul Virilio. Le musée des Arts décoratifs est encore le lieu du Centre
de création industrielle, même s’il est devenu un département du Centre
Pompidou en 1973. Il s’agit d’une initiative directe de François Mathey,
fondateur et directeur du CCI qui « portait un intérêt très vif aux
recherches <!-- comment cela s'appelle ce geste ? --> \[de Paul Virilio\] sur ce sujet, et <!-- ici aussi --> \[qu’il connaissait\] de
longue date[^2] ». Quelle peut être alors la pertinence, pour le CCI, le
Centre Pompidou ou François Mathey, de réaliser une exposition sur
l’architecture du littoral de la Seconde Guerre mondiale en 1975 ? une
année anniversaire ? Produire une exposition sur la guerre pour la
trentième année de l’armistice ? 1975 est également la dernière année de
Paul Virilio comme directeur de l’École spéciale d’architecture, comme
en témoignent les changements d’en-tête dans les courriers utilisés par
Virilio durant les négociations de l’exposition. Sa démission coïncide
avec, voire est la conséquence d’une modification certaine de ses
centres d’intérêt, le faisant délaisser peu à peu les questions
d’urbanisme et d’environnement pour les réflexions sur le progrès
technique et la vitesse. *Bunker archéologie* est donc aussi la
manifestation d’une volonté, celle de clôturer une étape dans la pensée
de Paul Virilio. La nécessité de réunion qu’implique la conception d’une
exposition apparaît comme l’occasion de clore une période de réflexion.
La présentation au public par l’exposition permet à Paul Virilio aussi
bien de synthétiser sa pensée que d’autoriser le visiteur à saisir
« l’occasion donnée à chacun de s’interroger sur les conséquences
pratiques et théoriques d’un jeu de la guerre[^3] ».

### c. Depuis la marge {#c-depuis-la-marge}

*Bunker archéologie* est une exposition personnelle. Elle naît d’une
expérience de vie, témoigne d’une thèse en architecture, expose des
photographies et est réalisée par Paul Virilio lui-même. Il est à la
fois l’analyste et le praticien. Aucun autre nom ne figure pour les
textes ni même pour la scénographie, hormis l’entreprise Volume Édouard
Maurel pour sa réalisation, qui collabore fréquemment avec le Centre
Pompidou pour le montage des expositions. *Bunker archéologie* est un
travail solitaire dont l’auteur protège le périmètre : « Paul Virilio,
auteur de l’exposition, déterminera avec le CCI la présentation de cette
exposition (format des documents, légendes, maquettes et présentation de
tout autre document), et l’animation autour de l’exposition
(organisation des colloques, débats, films, etc.)[^4]. » Il ne s’agit
donc pas d’une collaboration et Paul Virilio ne se présente pas non plus
en commissaire d’exposition comme Jean-François Lyotard, un autre
philosophe français, le fera pour l’exposition *Les Immatériaux* en 1985. *Bunker archéologie* se veut être la stricte présentation d’un
voyage en solitaire, de sa pensée et de ses images. Paul Virilio n’a par
ailleurs que très peu collaboré, d’autant plus dans sa pratique
architecturale. Même parlant d’Architecture Principe, il décrit sa
rencontre avec Claude Parent comme « la rencontre de deux marginaux
réunis par la solitude[^5] ». *Bunker archéologie* expose la solitude.
Autant celle de son auteur que celle de ces monstres de béton sur cette
immensité de sable, absurdes aujourd’hui, abandonnés, comme inadaptés à
l’Histoire qui a suivi son cours sans eux.

> Le sentiment le plus clair était encore celui de l’absence : l’immense plage de La Baule était déserte, nous étions moins d’une dizaine sur l’anse de sable blond, les rues étaient dépourvues de tout véhicule; c’était une frontière qu’une armée venait à peine d’abandonner et la signification de cette immensité marine était inséparable pour moi de cet aspect de champ de bataille déserté[^6].

Son enquête photographique est une enquête sur la marge, un voyage entre
ce qu’il appellera « la dernière frontière » d’un côté et les vestiges
du Mur de l’Atlantique de l’autre. *Bunker archéologie* dresse des murs,
supports des textes et des photographies, comme l’exposition de la
marge.

## II - Espace repoussoir {#ii-espace-repoussoir}

### a. Musée de la guerre {#musee-de-la-guerre}

Quel lieu pour exposer ces bunkers ? Un musée de la guerre qui prendrait
le contre-pied radical des musées de guerre. « Paul Virilio nous convie
à cette visite au <!-- qu'est-ce que c'est que ça (crochets flèches) ?--> “ musée de la guerre ” qu’abrite momentanément celui des Arts décoratifs[^7]. » Le caractère architectural et historique du
sujet de l’exposition transforme l’espace muséal. En faisant des Arts
décoratifs un « musée de la guerre », Paul Virilio plie l’espace à son
propos. Les « musées de la guerre » sont généralement dans une démarche
de reconstitution, aussi bien d’une chronologie, que des espaces (postes
de commandement, bases sous-marines, etc.) ou des costumes. Ces musées
tentent de retranscrire une forme de réalité de la guerre par
l’immersion dans l’époque. C’est notamment le cas à La Rochelle, ville
où a vécu Paul Virilio pendant de nombreuses années, où un ancien bunker
allemand a été réaménagé en musée de la Seconde Guerre mondiale. La
visite y est décrite comme une « plongée dans un univers d’Histoire et
d’émotions[^8] ». Seulement le lieu fait tout autant partie des
collections du musée, comme monument historique, que les pièces qui sont
exposées en son sein. *Bunker archéologie* prend place au musée des Arts
décoratifs dans le cadre d’une exposition dont « le montage est
déterminé pour l’itinérance[^9] » : une exposition éphémère de la guerre
dont l’immersion n’est pas dans le réalisme des pièces présentées mais
dans la réunion d’éléments permettant à Paul Virilio de raconter son
histoire. Le sujet post-Seconde Guerre mondiale ainsi que l’usage de la
photographie peuvent notamment évoquer l’exposition de 1955, *The Family of Man*[^10] qui s’était tenue au MoMA et dans le monde entier, mais au lieu d’un foisonnement enthousiaste et quelque peu benêt que Roland Barthes dénoncera avec vigueur[^11], c’est avec une rigueur, voire une certaine austérité que l’architecte organise le parcours. À la
juxtaposition d’une multitude de photographies sur un même mur qui
permettait de communiquer le message d’unification, de paix nécessaire à
la période de reconstruction des années 1950, on peut opposer la
juxtaposition prolifique mais implacable d’une démonstration vigoureuse.
<!-- faut-il changer de paragraphe ici ?-->
Ces musées, traducteurs des révolutions techniques de l’industrie de la
guerre, sont tout autant des lieux d’innovation. Même *The family of
Man*, évitant la stricte reconstitution, a nécessité, si ce n’est
l’invention, du moins l’adaptation de la technique d’impression afin
d’obtenir les photographies. Bien que beaucoup plus récents, les plans
de Daniel Libeskind pour le musée juif de Berlin[^12], datés de 1988,
dessinent un monstre technique. Proche de certaines compositions
futuristes, le Blitz[^13] témoigne de la complexité de l’édifice mais
également du conflit, de la vitesse, de l’impact lié à ce type de lieux.
Autant de sujets qui deviendront centraux dans la pensée de Paul
Virilio. Pourtant, avec *Bunker archéologie*, il opère un choix
radicalement opposé à une sophistication technique de l’espace. La
technicité nuit-elle à la diffusion et à la compréhension du message
selon Virilio ? Dans la volonté de produire un espace de réflexion, il
propose un dispositif minimaliste, sans écran ou images en mouvement. En
produisant un « musée de la guerre » au sein des Arts décoratifs, il
porte un discours peu évident, pleinement conscient, en plein Paris.
Apporter ces bunkers ensablés dans la capitale, c’est utiliser la
capacité de l’exposition à toucher les masses.

### b. Sainte-Bernadette du Banlay {#sainte-bernadette-du-banlay}

*Bunker archéologie* joue sur un décalage fort, un peu à la manière du
projet manifeste d’Architecture Principe[^14] (1963-1969), la cellule de
travail créée avec Claude Parent. L’architecture militaire n’a rien d’un
art décoratif. Paul Virilio inonde la galerie d’exposition de ses
photographies de bunkers pour profiter du contraste que lui offre le
lieu. Que ce soit dans ses écrits ou dans sa pratique architecturale, il
travaille l’inconfort, réhabilite ce qui dérange ; le sujet de sa thèse
consiste justement à voir les qualités architecturales voire culturelles
de cette période que l’on ne veut pas voir. En architecture, cette
recherche s’incarne dans la *fonction oblique* établie avec Claude
Parent au sein d’Architecture Principe[^15]. Une stratégie de l’effort
qui prend forme dans un projet, en 1966, dix ans avant *Bunker
archéologie*, avec l’église Sainte-Bernadette du Banlay à Nevers. 

Selon Paul Virilio, cette église est l’expérimentation architecturale de
l’architecture du bunker. À Sainte Bernadette la Vierge Marie est
apparue à Lourdes et le travail d’Architecture Principe tourne autour
d’une interprétation contemporaine de la grotte. Cette grotte à la fois
lieu des apparitions et de répulsion, où l’on redoute d’aller, se devait
d’être un signe de son époque. Virilio y ajoute le fait de « construire
dans la ville d’*Hiroshima mon amour*[^16] ». L’église est devenue
abri-atomique, est devenue bunker. Architecture Principe joue sur le
contraste du lieu et sur un contraste topographique : comme dans son
étude du littoral présentée à Paris, il pose un bunker en plein centre
de la France. Le regard que porte et montre Paul Virilio dans *Bunker
archéologie* est un regard d’architecte. Un regard sur les blocs, les
matériaux, les détails et les volumes. Ce qu’il photographie n’est pas
la guerre dans sa capacité à détruire. Il ne photographie pas les
immeubles de Nantes détruits par centaines, il regarde ce que la guerre
a construit. Sainte-Bernadette du Banlay est une architecture de son
temps parce qu’elle a vu ce que la guerre a construit. Alors on ne peut
plus construire comme avant.

<!-- ici : quelle règle d'espace et de ponctuation ?-->
### c. Exposition / Manifeste {#c-exposition-manifeste}

À l’origine de l’enquête de Paul Virilio sur l’architecture militaire,
une injustice. Celle d’une vision négative sur cette architecture.
« Nous ne sommes pas catastrophistes, au contraire c’est très
excitant[^17] », peut-il dire à Moebius. L’enjeu de la pensée de Paul
Virilio, qui était également celui d’Architecture Principe, est une
lutte. Après la Seconde Guerre mondiale, l’urgence est la
reconstruction. À Nantes, où Virilio a vécu durant cette période, plus
de 8000 immeubles sont rasés et la reconstruction de ces bâtiments se
fait dans une nécessaire immédiateté. Sous couvert de préceptes
modernistes, fleurissent de toutes parts des barres d’immeubles : « La
reconstruction est la poursuite des horreurs de la guerre. Si la guerre
a été totale, l’après-guerre est totalitaire[^18]. » C’est un modèle
d’architecture à la fois sériel et immédiat que qualifie sans concession
Virilio. Il s’agit là de la première critique qui nourrit cette
recherche. La seconde porte sur les Trente Glorieuses, elle est
théorisée dans le travail que le penseur mènera au sein d’Architecture
Principe : les arts ménagers endorment les usagers dans une époque du
confort. Une illusion de richesse qui ferme les yeux devant les
stigmates urbains de la guerre. Que ce soit la fonction oblique en
architecture ou l’exposition *Bunker archéologie* elle-même, la
production de l’espace passe par la sortie de l’illusion.

Comme devait l’être Sainte-Bernadette du Banlay, *Bunker archéologie* se
doit d’être une exposition en son temps. La partie « Séries et
Transformations » montre l’influence de la guerre sur l’architecture
contemporaine et leur mode de construction. C’est autant la
réhabilitation d’une histoire d’un type d’architecture comme objet
culturel qu’un lieu de critique et de réflexion autour de la manière de
faire la ville. « Cette exposition/manifeste nous invite à l’exigence
d’une meilleure information, voire d’un contrôle populaire de
l’industrie de la guerre[^19]. » Pour reprendre la définition d’André
Breton, un manifeste est « un exposé théorique lançant un mouvement
artistique, littéraire[^20] ». Contrairement à André Breton et aux
avant-gardes, Paul Virilio ne produit pas un manifeste expliquant la
nouvelle marche à suivre, une nouvelle manière de faire une exposition
ou de penser la guerre. À travers *Bunker archéologie*, il utilise
l’exposition pour réunir les clés de lecture d’une époque afin de
produire une architecture véritablement contemporaine, avec son temps,
ce qui signifie une architecture consciente de son Histoire. Il souhaite
éveiller les consciences sur ce qui est déjà présent, que l’on ne
regarde pas et qui est pourtant crucial à la compréhension de nos
villes, de nos campagnes et de nos littoraux. Avec cette exposition,
Paul Virilio agit comme ce que nous nommons aujourd’hui un lanceur
d’alerte.

## III - Montrer l’écriture {#iii-montrer-l-ecriture}

### a. Actualisation {#a-actualisation}

Le souci d’être de son temps correspond à la forme de l’exposition.
*Bunker archéologie* fait rentrer le temps long de l’Histoire, dans un
*moment*. Un moment court qui durera deux mois et demi. 

Son étude sur l’architecture militaire du Mur de l’Atlantique lui a pris
plus de dix ans. L’exposition aura lieu 17 ans après qu’il a pris ses
premières photographies. En ajoutant les rééditions du catalogue en 1991
puis la publication du texte, uniquement, aux éditions Galilée en 2008,
*Bunker archéologie* a cinquante ans. L’exposition n’est-elle pas alors
un moyen d’actualisation ? De saisir déjà les évolutions de l’histoire
durant ces 17 années, entre la première prise de vue et l’ouverture de
ce musée de la guerre ? L’introduction d’images de bâtiments
contemporains dans l’exposition montre la volonté de rester contemporain
malgré une étude commencée il a déjà plus de quinze ans. La
formalisation de cette pensée en mouvement permet à Paul Virilio
d’adapter, de moduler son propos aux différents bouleversements sociaux,
politiques, économiques ou urbanistiques. Une pensée par ailleurs
tellement en mouvement que l’exposition a connu une version itinérante
cette fois-ci dans de véritables « musées de la guerre » comme à
Thionville ou à Dieppe, et avait été proposée au Victoria and Albert
Museum de Londres, conduisant à une édition du catalogue en langue
anglaise.

La forme de l’exposition lui permet de témoigner du stade de sa
réflexion en 1975, avant de s’attacher à d’autres sujets. Les
différentes rééditions du texte montrent que son intérêt pour la
question persiste. Directeur de la collection « L’espace critique » chez
Galilée, il est lui-même à l’origine en 2008 d’une nouvelle publication
du texte. Le littoral réapparaît également en 2013. Comme des rémanences
d’une pensée qui dure sur plusieurs dizaines d’années. Si les bunkers
sont toujours présents sur nos plages et dans nos villes, leur charge
historique a disparu petit à petit. Les rééditions successives de ces
textes sont des rappels. Paul Virilio lutte contre la disparition de la
mémoire. Il lance de nouveau, parfois par un texte, parfois par une
exposition ou des photographies, l’alerte qu’il a lancée dès 1958.

Cependant *Bunker archéologie* reste la seule exposition directe de sa
pensée, en solitaire. Les expositions de la Fondation Cartier tournent
autour de sujets dans lesquels Virilio s’implique entièrement sans
qu’ils soient personnels pour autant. Elles regroupent différents
penseurs, architectes et artistes et Paul Virilio y joue un rôle
classique de commissaire d’exposition. *Bunker archéologie* est une
exposition d’auteur, pas de commissaire. Elle lui permet de réécrire sa
thèse, de l’actualiser par le mode de l’exposition. Cela implique une
modulation de l’écriture, et c’est proprement comme une nouvelle
publication dans l’espace, impliquant un rapport parallèle à la
construction du discours ou de la démonstration, du chapitrage[^21] et
d’autres contraintes, comme celle du volume et de l’image, à l’instar
des conceptions modernes de l’exposition décrite dans l’entre
deux-guerres par Adolf Behne.

### b. Le dessin graphique et le dessin littéraire {#b-le-dessin-graphique-et-le-dessin-litteraire}

Les plans de la scénographie sont à cet égard édifiants. Semblables à
une bande dessinée ou aux pellicules des photographies de Paul Virilio
sur lesquelles se succèdent les unes après les autres les vues de
bunkers. Deux grandes frises l’une au-dessus de l’autre, découpées en
cinq portions égales qui sont nommées par les têtes de chapitre
présentes dans le catalogue : Le paysage de guerre, Anthropomorphe et
zoomorphe, Les monuments du Péril, Séries et Transformations, Esthétique de la disparition.

Il est fréquent de voir des graphistes employer le vocabulaire propre à
l’architecture pour définir leur pratique. La page est alors un espace à
composer. Cette relation entre graphisme et architecture peut prendre
différentes formes. Le travail récurrent d’édition de Rem Koolhaas et
Bruce Mau[^22] montre l’association de ces deux disciplines par un
travail éditorial. L’architecture a en commun avec le graphisme qu’elle
est organisation d’une surface. Le travail pictural de Lazar Lissitzky
peut en témoigner. Ses règles de composition, de structure se retrouvent
également dans la grille de composition de graphistes suisses comme Otl
Aicher. Tout comme les planches de dessin d’Aicher, les deux frises
présentes sur les plans de *Bunker archéologie* sont quadrillées. S’il
est fréquent pour les graphistes de penser la page comme un espace, il
est, à l’inverse, plus rare de voir un architecte penser l’espace comme
une page. C’est pourtant ce que fait Paul Virilio avec cette exposition.
Toujours remarquables, les bons de commande des archives ne parlent pas
de plans ou de scénographie mais de mise en page. L’exposition/manifeste
est mise en page. L’espace est mis en page. Les plans de la scénographie
de l’architecte suivent l’écriture du penseur. « Nous tenons donc
essentiellement à ce que et l’exposition et le catalogue reflètent
fidèlement le sens de vos travaux et en soient une parfaite
illustration[^23]. » Chaque mur est une page, chaque salle est un
chapitre.

Les illustrations présentes dans l’exposition sont directement réalisées
sur les plans de l’exposition afin de les insérer dans la grille de
composition. Paul Virilio pratique lui-même le dessin et établit un
distingo : « Les deux dessins, le dessin graphique et le dessin
littéraire, provoquent des images, simplement, les images de la
littérature sont des images mentales[^24]. » Il se trouve que Paul
Virilio, également architecte, connaît et pratique le dessin
architectural, ce qu’il appelle plus haut le dessin graphique. Il
pratique le dessin qui produit non pas des images mentales mais des
projets. Il n’a pas mis fin à cette pensée topologique par le terrain,
il n’a pas ôté ses yeux d’architecte avec la séparation d’Architecture
Principe. « Nous ne nous sommes pas séparés par les petites histoires
mais bien par l’Histoire[^25] », cette même Histoire qu’il analyse et
expose dans *Bunker archéologie*.

### c. Un art révélationnaire {#c-un-art-revelationnaire}

L’exposition « est un art révélationnaire[^26] », affirme Paul Virilio.
En ce sens, *Bunker archéologie* révèle l’Histoire d’hier et
d’aujourd’hui, révèle un territoire, celui du littoral et de la
reconstruction et révèle les images que son dessin à la fois graphique
et littéraire produit. Son dessin d’architecte-penseur. La qualité
manifeste prend alors son sens : manifeste d’une écriture qui n’utilise
pas les mots mais la photographie, les plans, la composition, d’un
dessin dans la marge entre le graphique et le littéraire, qu’il aura
tendance à faire disparaître ou effacer mais qui reste fondateur. Comme
les photographies disparaissent des catalogues d’exposition, comme les
bunkers disparaissent dans le sable, le dessin mixte de Virilio ne sera
plus aussi marqué que dans cette exposition.

« L’architecte est celui qui fait le détour, qui s’occupe du détour,
c’est-à-dire l’environnement de la question[^27]. » Photographies, plans
de bunkers de la Seconde Guerre mondiale, architectures contemporaines,
images non plus mentales mais affichées, textes sur les pages produisent
l’environnement de la question : « L’espace militaire possède-t-il un
caractère permanent ? » C’est ici qu’il faut voir la qualité spatiale de
l’exposition où ces images, c’est-dire la question et sa réponse,
environnent littéralement le visiteur. L’espace du centre est vidé,
seuls les murs sont pleins. « Il ne s’agit pas, dans cette exposition,
de la représentation d’objets architecturaux mais de l’occasion donnée à
chacun de s’interroger sur les conséquences pratiques des théories d’un
jeu de la guerre[^28]. »

*Bunker archéologie* est l’exposition de la réflexion d’un
architecte/philosophe. Elle est le manifeste de l’état de sa pensée en
1975, après avoir terminé une recherche photographique, fondé puis
quitté un groupe d’architecture, et étant en train de quitter la
direction d’une école d’architecture. *Bunker archéologie* est une
conclusion. La proposition de François Mathey a permis à Paul Virilio de
fermer une pensée qu’il savait en fin de cycle. L’exposition, dans sa
double nécessité de réunion de documents et de présentation à un public,
stabilise sa pensée avant de tirer les fils de sa réflexion à venir.
S’il présente *Bunker archéologie* comme une fin, c’est qu’il connaît
déjà la suite. Alors il vend ses photographies de bunkers, il vend les
négatifs, les pellicules et les diapositives au Centre Pompidou. Ce
sujet, il ne le détient plus. Le musée prend le relais. Il cède ses
documents personnels à un espace clos et labyrinthique que sont les
archives du Centre Pompidou. Un espace de conservation où ses pellicules
seront préservées. Un espace d’où elles ne sont ressorties que par
quelques archéologues qui peu à peu disparaissent. Un espace comme un
bunker.

---

## Bibliographie {#bibliographie}

### Ouvrages {#ouvrages}

- BARTHES, Roland. *Mythologies*. Paris : Seuil, 2005, « Points Essais ».
- BAUDRILLARD, Jean. *L’effet Beaubourg : implosion et dissuasion*. Paris : Galilée, 1977.
- FOUCAULT, Michel. *Les mots et les choses. Une archéologie des sciences humaines*. Paris : Gallimard, 2005.
- GARRIC, Jean-Philippe. *Le livre et l’architecte : actes du colloque, Paris, 31 janvier – 2 février 2008*. Liège : Mardaga, 2011.
<!-- faut-il inverser Etienne et HENRY ? -->
- GRALL, Guillaume et Étienne HENRY. *Étapes 195 : Quelque part entre graphisme et architecture…*. Paris : Pyramid, 2010.
- LAUXEROIS, Jean. *L’Utopie Beaubourg : vingt ans après*. Paris : Bibliothèque publique d’information, Centre Georges Pompidou, 1996.
- Office for Metropolitan Architecture, KOOLHAAS, Rem, MAU, Bruce. *S, M, L, XL:Small, medium, large, extra-large.* New York : Monacelli Press, 1995.
- PARENT, Claude et Paul VIRILIO. *Architecture principe, 1966 et 1996*. Paris : Éditions de l’Imprimeur, 1996.
- PARENT, Claude. *Portraits, impressionnistes et véridiques d’architectes*. Paris : Norma, 2005.
- PONGE, Francis. *L’écrit Beaubourg*. Paris : Centre Georges Pompidou, 1977.
- PUINEUF (de), Sonia. *Architecture et typographie, atelier de recherche éditoriale : quelques approches historiques*. Paris : B42, 2011.
- VIRILIO, Paul. *Bunker archéologie*. Catalogue d’exposition, Centre Georges Pompidou, Paris, 10 décembre 1975 – 29 février 1976, Paris : Centre Georges Pompidou-CCI, 1975.
- VIRILIO, Paul. *Bunker archéologie*. Paris : Éditions du Demi-Cercle, 1992.
- VIRILIO, Paul. *Bunker archéologie*. Paris : Galilée, 2008.
- VIRILIO, Paul, et Raymond DEPARDON. *Terre natale : ailleurs commence ici*. Paris : Actes Sud, Fondation Cartier pour l’art contemporain, 2010.
- VIRILIO, Paul. *La pensée exposée : textes et entretiens pour la Fondation Cartier pour l’art contemporain*. Paris : Actes Sud, Fondation Cartier pour l’art contemporain, 2012.
- VIRILIO, Paul, et Jean-Louis VIOLEAU. *Le littoral, la dernière frontière : entretien avec Jean-Louis Violeau*. Paris : Sens et Tonka, 2013.

### Filmographie {#filmographie}

- COUDERT, Gilles. *Claude Parent et Paul Virilio – Grandes conférences*, a.p.r.è.s. éditions, 2009.
- PAOLI, Stéphane. *Paul Virilio : Penser la vitesse* (DVA). Documentaire, 90 min. Paris : Arte France, La Générale de Production, 2008.

### Exposition {#exposition}

- *Graphisme… Architecture*, Maison de l’architecture et de la ville du Nord-Pas de Calais, Euralille, 2010.

### Archives {#archives}

- *Bunker archéologie*, Paul Virilio. CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.
- *Bunker archéologie*, Paul Virilio. CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.

---

## Table des figures {#table-des-figures}
<!-- attention au nom des fichiers images : impérativement renommer mécaniquement (datetime-cote?)   escapeSpecialChars(and spaces)-->


[Figure 1]{#fig-1}

![Vue partielle du plan de l’exposition *Bunker Archéologie*. Format papier A0. CCI 20.1-10, n°94033/074, Production, Volume Edouard Maurel, 1975. CNAC Georges Pompidou, Service des Archives.](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/PLAN_VIRILIO_1.jpg)

---

[Figure 2]{#fig-2}

![alt](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/PLAN_VIRILIO1_1.jpg)

---

[Figure 3]{#fig-3}

![alt](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/tof1_1.jpg)

---

[Figure 4]{#fig-4}

![alt](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/tof2_1.jpg)

---

[Figure 5]{#fig-5}

![alt](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/tof3_1.jpg)

---

[Figure 6]{#fig-6}

![alt](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/tof4_1.jpg)

---

[Figure 7]{#fig-7}

![alt](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/tof5_1.jpg)

---

[Figure 8]{#fig-8}

![alt](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/tof6_1.jpg)

---

[Figure 9]{#fig-9}

![alt](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/vue-expo1_1.jpg)

---

[Figure 10]{#fig-10}

![alt](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/content/602533ee0971a/medias/vue-expo2_1.jpg)



<!-- style (italique et souligné) dans les notes -->
<!-- automatiser ibid. et op.cit. ? -->
<!-- faut-il mettre ibid. en italique ? -->
[^1]: Paul VIRILIO. *Bunker archéologie*. Paris : Galilée, 2008.
[^2]: François MATHEY. Courrier du 20 août 1975 à l’attention de Paul Virilio. Archives *Bunker archéologie*, Paul Virilio, CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.
[^3]: Paul VIRILIO. Communiqué de presse de l’exposition *Bunker archéologie*. Archives *Bunker archéologie*, Paul Virilio, CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.
[^4]: Projet de lettre d’engagement du CCI vis-à-vis de Paul Virilio. Archives *Bunker archéologie*, Paul Virilio, CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.
[^5]: Gilles COUDERT. *Claude Parent et Paul Virilio*, Grandes conférences (livre DVD/digipack), a.p.r.è.s. éditions, 2009.
[^6]: Paul VIRILIO. *Bunker archéologie*, op. cit.
[^7]: Paul VIRILIO. Communiqué de presse de l’exposition *Bunker archéologie*. Archives Bunker archéologie, Paul Virilio, CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.
[^8]: Communication du « Bunker de La Rochelle », source internet.
[^9]: Projet de lettre d’engagement du CCI vis-à-vis de Paul Virilio. Archives *Bunker archéologie*, Paul Virilio, CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.
[^10]: *The Family of Man*, The Museum of Modern Art, New York, 24 janvier – 8 mai 1955.
[^11]: Roland BARTHES. *Mythologies*. Paris : Seuil, 2005, « Points Essais ».
[^12]: Les plans sont actuellement visibles dans la collection permanente du Centre Pompidou.
[^13]: « Blitz » signifie « éclair » en allemand. Ce surnom est dû à la forme en éclair du musée.
[^14]: Groupe de recherche autour des questions architecturales, qui développera notamment la notion de la « fonction oblique ».
[^15]: Claude PARENT et Paul VIRILIO. *Architecture principe, 1966 et 1996*. Paris : Éd. de l’Imprimeur, 1996.
[^16]: Allusion que Virilio fait au film d’Alain Resnais (1959), sur un scénario de Marguerite Duras, lors de la rencontre qui le réunit à Claude Parent à Nevers en 1996.
[^17]: Stéphane PAOLI. *Paul Virilio : Penser la vitesse* (DVD), Paris : ARTE France, La Générale de Production, 2008.
[^18]: Gilles COUDERT. *Claude Parent et Paul Virilio – Grandes conférences* (livre DVD/digipack), a.p.r.è.s. éditions, 2009.
[^19]: Paul VIRILIO. Communiqué de presse de l’exposition *Bunker archéologie*. Archives Bunker archéologie, Paul Virilio, CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.
<!-- 2 fois manifeste du surréalisme en italique ? WTF-->
[^20]: André BRETON. *Manifeste du surréalisme*, préface de Poisson soluble [1924], in *Manifestes du surréalisme*, Paris : Jean-Jacques Pauvert, 1962.
[^21]: « Ici, l’exposition signifie bien plus le cheminement organisé du visiteur attentif. Et ce cheminement le long d’objets précis, selon une direction et une succession déterminées et claires, et identiques au cheminement de pensées l’exposant. […] en bref c’est un cheminement de pensées qu’il suit, dans tous ses détours logiques. » Adolf BEHNE. « AHAG – Ausstellung », *Internationale Revue i10*, vol. 12, n° 17-18, 1928 (séminaire d’Histoire du design : « Design - Display : une autre histoire des expositions », 2017/2018, M2R ENS Paris-Saclay).
[^22]: *Office for Metropolitan Architecture, Rem KOOLHAAS, Bruce MAU. S, M, X, XL: Small, medium, large, extra-large*. New York : Monacelli Press, 1995.
<!-- ici on parle de l'archive bunker arch. pas de l'expo : italique à enlever ? -->
[^23]: François MATHEY. Courrier du 20 août 1975 à l’attention de Paul Virilio. Archive *Bunker archéologie*, Paul Virilio, CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.
[^24]: Paul VIRILIO. *La pensée exposée : textes et entretiens pour la Fondation Cartier pour l’art contemporain*. Paris : Actes Sud, Fondation Cartier pour l’art contemporain, 2012. Discussion avec le dessinateur Moebius.
[^25]: Paul Virilio fait ici référence aux événements de Mai 68 qui l’ont attiré vers des actions politiques et manifestantes, notamment la prise de l’Odéon, et éloigné des recherches d’Architecture Principe. Gilles COUDERT. *Claude Parent et Paul Virilio – Grandes conférences* (livre DVD/digipack), a.p.r.è.s. éditions, 2009.
[^26]: Paul VIRILIO. *La pensée exposée : textes et entretiens pour la Fondation Cartier pour l’art contemporain*, op. cit.
[^27]: Ibid.
[^28]: Paul VIRILIO. Communiqué de presse de l’exposition *Bunker archéologie*. Archive *Bunker archéologie*, Paul Virilio, CCI 20.1-10 n° 94033/074, Centre Pompidou, Paris, 1975.
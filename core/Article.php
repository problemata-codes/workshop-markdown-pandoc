<?php

/*  

    Création d'un objet Article
    à partir d'un chemin relatif à la racine du dossier.

    @property string $path Le chemin vers le dossier de l'article
    @property string $id
    @property string $titre
    
*/

class Article {
    public $path;
    public $properties = array(
        'id' => '', 
        'titre' => '',
        'mediateur' => '',
    );

    public function __construct($path) {        
        $this->path = $path;
        $this->setValues();
    }

    public function setValues() {

        // récupération du fichier json du dossier
        $pattern = "{$this->path}/*.json";
        $json_path = glob($pattern)[0];

        $json_data = json_decode(file_get_contents($json_path));

        foreach($json_data as $key => $value) {
            if (isset($this->properties[$key])) {
                $this->properties[$key] = $value;
            }
        }        
    }

    /*
        Retourne le contenu du .md du dossier
    */
    public function getMarkdown() {

        // récupération du fichier md du dossier
        $pattern = "{$this->path}/*.md";
        $md_path = glob($pattern)[0];

        $md_data = file_get_contents($md_path);

        return $md_data;
    }

    /*
        Retourne le html à partir du .md
    */
    public function getHTML() {

        // récupération du fichier md du dossier
        $md_data = $this->getMarkdown();

        $parser = "pandoc";

        if ($parser == "pandoc") {

            // récupération du fichier md du dossier
            $pattern = "{$this->path}/*.md";
            $md_path = glob($pattern)[0];
            
            // markdown_in_html_blocks
            // raw_html
            $command = "pandoc -f markdown -t html ".$md_path;
            $contenu = shell_exec($command);
        } else {
            $MyParsedown = new MyParsedown();
            $contenu = $MyParsedown->text($md_data);
        }

        return $contenu;
    }

    public function get($key) {

        switch($key) {
            case 'markdown':
                $value = $this->getMarkdown();
                break;
            case 'html':
                $value = $this->getHTML();
                break;

            default:
                if ($key && isset($this->properties[(string)$key])) return $this->properties[$key];
        }

        return $value;
    }

    public function __get($key) {
        return $this->get($key); 
    }
}
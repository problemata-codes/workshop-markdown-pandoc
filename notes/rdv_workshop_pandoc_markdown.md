## workshop pandoc markdown
### jeudi 11.02.2021 -> vendredi 12.02.2021
### 13:30 -> 17:00-17:15 & 9:30-13:30 -> 12:30-17:00
#### cg, ml, vm, jdb

# idée globale : transfert de word vers le site en html
# objectif : avoir une guideline pour les auteurs

### modalités
- 3 cobayes : Marie Lejault, Catherine Geel et Stéphanie Geel
- les personnes arrivent avec des word, il l'upload, c'est parsé en markdown, on peut le retravailler, puis le voir
- sur discord

### préparation
- matériaux qu'il nous faut pour faire travailler les participants :
	- principalement des essais (étudiants du CCI)
	- il nous faut des images
		- pas sous forme de lien distant
		- viewer image ? = modalités d'affichage des images indépendemment de leurs contextes ou de les sortir de leur contexte pour les étudier

### à venir
- intégrer les statiques
	- CGU
	- RGPD / cookies = on peut aider
		- décrire ce que l'on stocke comme données personnelles
	- questions techniques = nous
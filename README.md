# workshop markdown pandoc

Outil d'écriture interne pour écrire des articles en .md et les générer en html ou en pdf. Visible ici : [http://problemata.org/ws/workshop-markdown-pandoc/](http://problemata.org/ws/workshop-markdown-pandoc/) (prototype du Massive Writer)



## To do

- ?page=
	- index
		- trier / filtrer / rechercher dans les résultats
	- docx
	- editor
	- detail
		- ?format=
			- html
				- insérer la balise *article* rendue dans la div #html 
			- pdf
				- insérer le pdf rendu dans l'iframe de la div #pdf
- scripts
	- docx_to_markdown
		- il faut que tu y insères le contenu de l'ancien fichier *docxconverter.php* (que je n'ai plus, je suis désolé!)
		- ça ne marche pas chez moi mais je pense que c'est parce que je n'ai pas installé les requirements listés ci-dessous
	- markdown_to_html
		- l'id arrive en méthode *GET* arg=id, il ne faut plus que retrouver le fichier depuis l'id et lancer pandoc et retourner les résultats
	- markdown_to_pdf
		- l'id arrive en méthode *GET* arg=id, il ne faut plus que retrouver le fichier depuis l'id et lancer pandoc et retourner les résultats

## Requirements

- php.ini : file_uploads = On
- droits écriture Apache sur content/
- pandoc installé et dans le PATH
- php : testé avec php 8.0.1
- pandoc : testé avec pandoc 2.11.3

- + installer LaTex (pour la génération de pdf) (https://www.latex-project.org/get/)
	- windows : https://www.latex-project.org/get/
<!-- 
	jdb
		pandoc : 
			C:\Users\jdb>pandoc --version
			pandoc 2.11.4
			Compiled with pandoc-types 1.22, texmath 0.12.1, skylighting 0.10.2,
			citeproc 0.3.0.5, ipynb 0.1.0.1
			User data directory: C:\Users\jdb\AppData\Roaming\pandoc
			Copyright (C) 2006-2021 John MacFarlane. Web:  https://pandoc.org
			This is free software; see the source for copying conditions. There is no
			warranty, not even for merchantability or fitness for a particular purpose.
			---
			C:\xampp\htdocs\sites\problemata\problemata_codes\workshop-markdown-pandoc>pandoc --version
			pandoc 2.11.4
			Compiled with pandoc-types 1.22, texmath 0.12.1, skylighting 0.10.2,
			citeproc 0.3.0.5, ipynb 0.1.0.1
			User data directory: C:\Users\jdb\AppData\Roaming\pandoc
			Copyright (C) 2006-2021 John MacFarlane. Web:  https://pandoc.org
			This is free software; see the source for copying conditions. There is no
			warranty, not even for merchantability or fitness for a particular purpose.
 -->
